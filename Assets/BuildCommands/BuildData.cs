using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="BuildData", menuName = "Build Data")]
public class BuildData : ScriptableObject
{
    [Header("Butler Info")]
    public bool uploadBuild = true;
    public string studioName;
    public string projectName;
    public List<string> ignore;

    [Header("Build Info")]
    public bool updateBuildInfo = true;
    public int buildNumber;

    [Header("Git Info")]
    public bool updateGitInfo = true;
    public string sha;
    public string branch_name;
}

