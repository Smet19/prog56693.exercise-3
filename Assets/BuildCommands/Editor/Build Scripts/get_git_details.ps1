[hashtable]$Return = @{}

$branch_name = git rev-parse --abbrev-ref HEAD
$sha = git log --pretty=format:'%h' -n 1

$Return.branch_name = $branch_name
$Return.sha = $sha
Return $Return