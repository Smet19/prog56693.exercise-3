using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.Build;
using UnityEditor.Build.Reporting;
using UnityEditor.Callbacks;
using UnityEngine;

public class BuildCommands : IPreprocessBuildWithReport, IPostprocessBuildWithReport
{
    public int callbackOrder => 1;
    public static string GetBuildCommandPath()
    {
        string[] res = System.IO.Directory.GetFiles(Application.dataPath, "BuildCommands.cs", System.IO.SearchOption.AllDirectories);

        if (res.Length == 0)
        {
            Debug.LogError("Unable to find BuildCommands.cs");
            return null;
        }

        string path = System.IO.Path.GetDirectoryName(res[0]);
        path = path.Replace("\\", "/");
        path = path.Replace(Application.dataPath, "");
        path = path.Replace("Editor", "");

        return path;
    }

    public void OnPreprocessBuild(BuildReport report)
    {
        string assetPath = GetBuildCommandPath();

        BuildData buildData = (BuildData)AssetDatabase.LoadAssetAtPath("Assets" + assetPath + "BuildData.asset", typeof(BuildData));

        if (buildData != null)
        {
            bool saveAsset = false;

            if(buildData.updateBuildInfo)
            {
                saveAsset = true;
                buildData.buildNumber++;
            }

            if(buildData.updateGitInfo)
            {
                string command = "\"" + Application.dataPath + assetPath + "Editor/Build Scripts/get_git_details.ps1\"";

                command = "-NoExit -ExecutionPolicy ByPass -File" + command;

                System.Diagnostics.Process process = new System.Diagnostics.Process();
                process.StartInfo = new System.Diagnostics.ProcessStartInfo("powershell.exe", command);
                process.StartInfo.RedirectStandardOutput = true;
                process.StartInfo.UseShellExecute = false;
                process.Start();

                string standardOutput = "";
                while(!process.HasExited)
                {
                    standardOutput += process.StandardOutput.ReadToEnd();
                }
                standardOutput += process.StandardOutput.ReadToEnd();
                process.Close();

                string[] outputLines = standardOutput.Split('\n');

                foreach(var line in outputLines)
                {
                    if(line.Contains("sha"))
                    {
                        string shaLine = line.Replace("sha", "");
                        shaLine = shaLine.Replace(" ", "");
                        buildData.sha = shaLine;
                    }

                    if(line.Contains("branch_name"))
                    {
                        string branch_name = line.Replace("branch_name", "");
                        branch_name = branch_name.Replace(" ", "");
                        buildData.branch_name = branch_name;
                    }
                }
                saveAsset = true;
            }

            if(saveAsset)
            {
                EditorUtility.SetDirty(buildData);
                AssetDatabase.SaveAssets();
            }
        }
    }

    public void OnPostprocessBuild(BuildReport report)
    {
        string assetPath = GetBuildCommandPath();
        BuildData buildData = (BuildData)AssetDatabase.LoadAssetAtPath("Assets" + assetPath + "BuildData.asset", typeof(BuildData));

        if (buildData != null && buildData.uploadBuild)
        {
            try 
            {
                string outPath = System.IO.Path.GetDirectoryName(report.summary.outputPath);

                string butlerPath = "\'butler.exe\'";
                //string butlerPath = "\'" + Application.dataPath + assetPath + "Editor/Butler/butler.exe\'";

                string platform = "";
                switch (report.summary.platform)
                {
                    case BuildTarget.StandaloneWindows:
                    case BuildTarget.StandaloneWindows64:
                        platform = "windows";
                        break;

                    case BuildTarget.StandaloneOSX:
                        platform = "osx";
                        break;

                    case BuildTarget.iOS:
                        platform = "iOS";
                        break;

                    case BuildTarget.WebGL:
                        platform = "webGL";
                        break;

                    default:
                        platform = "build";
                        break;
                }

                string command = "& " + butlerPath + " -v";

                foreach(string ignore in buildData.ignore)
                {
                    command += " --ignore \'" + ignore + "\'";
                }

                string pushFolder = "\"" + outPath + "\"";

                command += " push \'" + pushFolder + "\' " + buildData.studioName + "/" 
                    + buildData.projectName + ":" + platform + " --userversion " + buildData.buildNumber;

                command = "-NoExit -Command " + command;
                //command = "-Command " + command;
                System.Diagnostics.Process process = System.Diagnostics.Process.Start("powershell.exe", command);

                process.WaitForExit();
                process.Close();
            }
            catch (System.Exception ex)
            {
                Debug.LogError("Error uploading buld to butler :" + ex.Message);
            }
        }
    }
}